﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
Push-Location -Path $here

"# " + (Split-Path -Leaf $MyInvocation.MyCommand.Path) | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host

"## " + 'Start Logging...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
$LogFolderPath = "$here\Log" | Resolve-Path
$LogFileName = (Get-Date).ToString("yyyyMMdd-HHmmss") + "_Log.txt"
$LogFilePath = $LogFolderPath | Join-Path -ChildPath $LogFileName
Start-Transcript -Path $LogFilePath


"## " + 'Import modules...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
$ProjectFolderPath = "$here\..\" | Resolve-Path

foreach ($PackageName in @("EDashboard","PlotlyPs")) {
    $PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
    Import-Module -Name $PackagePath -Force
}


"## " + 'Creating HTML file...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
$InputFolderPath = "$here\Input" | Resolve-Path
$OutputFolderPath = "$here\Output" | Resolve-Path

$CsvFilePath = $InputFolderPath | Join-Path -ChildPath "sunrise-sunset.csv"
$PageFilePath = $OutputFolderPath | Join-Path -ChildPath "sunrise-sunset.html"

$DXTHtmlArgs = @{
    "DateProperty" = "Date"
    "StartProperty" = "StartTime"
    "EndProperty" = "EndTime"
    "TargetPeriod" = @([datetime]::new(2022,1,1), [datetime]::new(2022,12,31))
    "ComparePeriod" = @([datetime]::new(2021,1,1), [datetime]::new(2021,12,31))
    "Title" = "Sunrise to sunset KPI Dashboard"
    "Description" = @(
        "東京の 2022年1月の日の出、日の入り、日中時間"
        ""
        "最終更新： 2022/01/10"
    )
    "KPINames" = @("日の出", "日の入り", "日中時間")
    "PlotlyUrl" = "https://cdn.plot.ly/plotly-1.50.0.min.js"
}

Import-EDashboardDailyPeriodCsv -Path $CsvFilePath |
New-PlotlyDailyPeriodKPIDashboardHtml @DXTHtmlArgs |
Out-File -FilePath $PageFilePath -Encoding "utf8"

"## " + 'End Logging...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
Stop-Transcript

Read-Host -Prompt ("Fin! Press enter to exit...")
