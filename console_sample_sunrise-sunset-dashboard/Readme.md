# Sample_sunrise-sunset-dashboard

EDashboardの使用方法を簡単に紹介。

東京の日毎の日の出時刻、日の入り時刻
日の出から日の入りまでの時間をダッシュボードに表示する。

## 準備

e-dashboard の ./src 配下に [plotly-ps](https://gitlab.com/2q3ridcz/plotly-ps) のソースフォルダ (./src/PlotlyPs フォルダ) を配置する。

フォルダ構成は下記のようになる。(抜粋。)

- e-dashboard
    - console_sample_sunrise-sunset-dashboard
    - src
        - EDashboard
            - EDashboard.psm1
        - PlotlyPs
            - PlotlyPs.psm1

## 引用文献

入力データ「sunrise-sunset.csv」は「国立天文台ホームページより引用」。

[サイト利用規定：国立天文台 暦計算室](https://eco.mtk.nao.ac.jp/koyomi/site/) の指定に従い引用文を記載。

ちなみに、データはそれぞれ下記ページより転記。

2022年1月分：  [日の出入り＠東京(東京都) 令和 4年(2022)01月 - 国立天文台暦計算室](https://eco.mtk.nao.ac.jp/koyomi/dni/2022/s1301.html)

2021年1月分：  [日の出入り＠東京(東京都) 令和 3年(2021)01月 - 国立天文台暦計算室](https://eco.mtk.nao.ac.jp/koyomi/dni/2021/s1301.html)
