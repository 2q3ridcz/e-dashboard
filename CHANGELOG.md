# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Fixed
- Fix KeyAndPeriodList's null key issue
- Fix Export-KeyAndPeriodListCsv pipline input issue

## [0.3.0] - 2022-04-26
### Added
- Export-KeyAndPeriodListCsv
- Import-KeyAndPeriodListCsv

## [0.2.0] - 2022-04-09
### Added
- Export-EDashboardDailyTimeAndAmountCsv
- Import-EDashboardDailyTimeAndAmountCsv

## [0.1.0] - 2022-02-19
### Added
- Export-EDashboardDailyPeriodCsv
- Import-EDashboardDailyPeriodCsv

## [0.0.0] - 2022-01-15
### Added
- Created new project. No features yet.

[Unreleased]: https://gitlab.com/2q3ridcz/e-dashboard/-/compare/v0.3.0...main
[0.3.0]: https://gitlab.com/2q3ridcz/e-dashboard/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/2q3ridcz/e-dashboard/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/2q3ridcz/e-dashboard/-/compare/v0.0.0...v0.1.0
[0.0.0]: https://gitlab.com/2q3ridcz/e-dashboard/-/tree/v0.0.0
