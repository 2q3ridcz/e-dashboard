﻿[System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSAvoidUsingWriteHost", "")]
Param( [bool]$CICDMode = $False )

"## " + 'Start Logging...: ' + $CICDMode | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$LogFolderPath = "$here\Output" | Resolve-Path
$LogFileName = (Get-Date).ToString("yyyyMMdd-HHmmss") + "_Log.txt"
If ( Test-Path -Path $LogFolderPath ) {} Else { New-Item -Path $LogFolderPath -ItemType Container }
$LogFilePath = $LogFolderPath | Join-Path -ChildPath $LogFileName
Start-Transcript -Path $LogFilePath

"## " + 'Displaying parameters...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
Write-Host('CI/CD Mode: ' + $CICDMode)
Write-Host("")

"## " + 'Displaying environment...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
$PSVersionTable | Format-Table
Get-Module -ListAvailable Pester, PSScriptAnalyzer | Format-Table
Write-Host("")

"## " + 'Preparing for test...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path
$SrcFolderPath = $ProjectFolderPath | Join-Path -ChildPath "src"
$TestsFolderPath = $ProjectFolderPath | Join-Path -ChildPath "tests"

Write-Host("Here  : " + $here)
Write-Host("Source: " + $SrcFolderPath)
Write-Host("Test  : " + $TestsFolderPath)

$TestResultFolderPath = "$here\Output" | Resolve-Path
$TestResultFileName = "TestResult.xml"
$TestResultFilePath = $TestResultFolderPath | Join-Path -ChildPath $TestResultFileName

# $CodeCoverageFileName = $Date.ToSTring("yyyyMMdd-HHmmss") + "_" + "CodeCoverage.xml"
# $CodeCoverageFilePath = $TestResultFolderPath | Join-Path -ChildPath $CodeCoverageFileName

$SrcFileFullNames =
Get-ChildItem -Path $SrcFolderPath -Recurse |
?{ @(".ps1", ".psm1") -contains $_.Extension } |
%{ $_.FullName }

$TestResult = Invoke-Pester `
    -PassThru `
    -Script $TestsFolderPath `
    -OutputFile $TestResultFilePath `
    -OutputFormat NUnitXML `
    -CodeCoverage $SrcFileFullNames
    # -CodeCoverageOutputFile $CodeCoverageFilePath

"## " + 'End Logging...: ' + $CICDMode | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
Stop-Transcript

if (-Not $CICDMode) {
    Read-Host ("Press enter to exit...") | Out-Null
}

if ($TestResult.FailedCount -gt 0) {
    ### Return to make GitLab CI job fail.
    EXIT 1
}
