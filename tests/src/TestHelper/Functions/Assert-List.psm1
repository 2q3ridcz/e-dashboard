function Assert-List {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True)]
        [AllowEmptyCollection()]
        [object[]]
        $Result
        ,
        [Parameter(Mandatory=$True)]
        [AllowEmptyCollection()]
        [object[]]
        $Expect
    )
    begin {}
    process {
        $Result.Length | Should Be $Expect.Length

        for ($i = 0; $i -lt $Result.Length; $i++) {
            $Result[$i] | Should Be $Expect[$i]
        }
    }
    end {}
}
