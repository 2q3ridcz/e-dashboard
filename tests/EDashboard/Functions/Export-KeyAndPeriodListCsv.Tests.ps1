$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path


Describe "Export-KeyAndPeriodListCsv" {
    Context "Operation verification" {
        BeforeEach {
            $InputObject = @(@(
                @{"Key1" = "Key1"; "Key2" = "Key2"; "ValidFrom" = "0"; "ValidTo" = "99991231"; "Value1" = "Value1"; "Value2" = "Value2" }
                @{"Key1" = "Key3"; "Key2" = "Key4"; "ValidFrom" = "0"; "ValidTo" = "99991231"; "Value1" = "Value1"; "Value2" = "Value2" }
            ) | %{ New-Object -TypeName psobject -Property $_ })
            $FuncParam = @{
                "InputObject" = $InputObject
                "Path" = "Path"
                "Keys" = @("Key1","Key2")
                "BeginProperty" = "ValidFrom"
                "EndProperty" = "ValidTo"
                "PropertyOrder" = @("Key1","Key2","ValidFrom","ValidTo","Value1","Value2")
            }
        }

        It "Throws when InputObject is null" {
            $Path = "$TestDrive\$("Throws when InputObject is null".replace(" ","-")).csv"
            $FuncParam["Path"] = $Path
            $FuncParam["InputObject"] = $Null
            {Export-KeyAndPeriodListCsv @FuncParam} | Should Throw
        }

        It "Throws when Path is null" {
            $FuncParam["Path"] = $Null
            {Export-KeyAndPeriodListCsv @FuncParam} | Should Throw
        }

        It "Creates a file" {
            $Path = "$TestDrive\$("Creates a file".replace(" ","-")).csv"
            $FuncParam["Path"] = $Path
            $Path | Should Not Exist
            $Result = Export-KeyAndPeriodListCsv @FuncParam
            $Path | Should Exist
            $Result | Should BeNullOrEmpty
            @(Get-Content -Path $Path -Encoding Default).Length | Should Be 3
        }

        It "Accepts InputObject from pipeline" {
            $Path = "$TestDrive\$("Accepts InputObject from pipeline".replace(" ","-")).csv"
            $FuncParam["Path"] = $Path
            $Path | Should Not Exist
            $FuncParam2 = @{
                "Path" = $Path
                "Keys" = $FuncParam["Keys"]
                "BeginProperty" = $FuncParam["BeginProperty"]
                "EndProperty" = $FuncParam["EndProperty"]
                "PropertyOrder" = $FuncParam["PropertyOrder"]
            }
            $Result = $InputObject | Export-KeyAndPeriodListCsv @FuncParam2
            $Path | Should Exist
            $Result | Should BeNullOrEmpty
            @(Get-Content -Path $Path -Encoding Default).Length | Should Be 3
        }
    }
}
