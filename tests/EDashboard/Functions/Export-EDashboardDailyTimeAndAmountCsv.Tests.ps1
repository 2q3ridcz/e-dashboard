$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path


Describe "Export-EDashboardDailyTimeAndAmountCsv" {
    Context "Operation verification" {
        BeforeEach {
            $InputObject = New-Object -TypeName psobject -Property @{
                "Date" = [datetime]::new(2021,1,1)
                "Time" = [datetime]::new(1,1,1,6,51,0)
                "Amount" = 98765
                "Note" = "Note"
            }
        }

        It "Throws when InputObject is null" {
            $Path = "$TestDrive\$("Throws when InputObject is null".replace(" ","-")).csv"
            {Export-EDashboardDailyTimeAndAmountCsv -InputObject $Null -Path $Path} | Should Throw
        }

        It "Throws when Path is null" {
            {Export-EDashboardDailyTimeAndAmountCsv -InputObject $InputObject -Path $Null} | Should Throw
        }

        It "Creates a file" {
            $Path = "$TestDrive\$("Returns DailyTimeAndAmount object file content is valid".replace(" ","-")).csv"
            $Path | Should Not Exist
            $Result = Export-EDashboardDailyTimeAndAmountCsv -InputObject $InputObject -Path $Path
            $Path | Should Exist
            $Result | Should BeNullOrEmpty
        }

        It "Accepts InputObject from pipeline" {
            $Path = "$TestDrive\$("Accepts InputObject from pipeline".replace(" ","-")).csv"
            $Path | Should Not Exist
            $Result = $InputObject | Export-EDashboardDailyTimeAndAmountCsv -Path $Path
            $Path | Should Exist
            $Result | Should BeNullOrEmpty
        }
    }
}
