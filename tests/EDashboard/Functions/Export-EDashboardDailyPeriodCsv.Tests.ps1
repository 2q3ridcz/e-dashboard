$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path


Describe "Export-EDashboardDailyPeriodCsv" {
    Context "Operation verification" {
        BeforeEach {
            $InputObject = New-Object -TypeName psobject -Property @{
                "Date" = [datetime]::new(2021,1,1)
                "StartTime" = [datetime]::new(1,1,1,6,51,0)
                "EndTime" = [datetime]::new(1,1,1,16,39,0)
                "Note" = "Note"
            }
        }

        It "Throws when InputObject is null" {
            $Path = "$TestDrive\$("Throws when InputObject is null".replace(" ","-")).csv"
            {Export-EDashboardDailyPeriodCsv -InputObject $Null -Path $Path} | Should Throw
        }

        It "Throws when Path is null" {
            {Export-EDashboardDailyPeriodCsv -InputObject $InputObject -Path $Null} | Should Throw
        }

        It "Creates a file" {
            $Path = "$TestDrive\$("Returns DailyPeriod object file content is valid".replace(" ","-")).csv"
            $Path | Should Not Exist
            $Result = Export-EDashboardDailyPeriodCsv -InputObject $InputObject -Path $Path
            $Path | Should Exist
            $Result | Should BeNullOrEmpty
        }

        It "Accepts InputObject from pipeline" {
            $Path = "$TestDrive\$("Accepts InputObject from pipeline".replace(" ","-")).csv"
            $Path | Should Not Exist
            $Result = $InputObject | Export-EDashboardDailyPeriodCsv -Path $Path
            $Path | Should Exist
            $Result | Should BeNullOrEmpty
        }
    }
}
