$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path


Describe "Import-KeyAndPeriodListCsv" {
    Context "Operation verification" {
        BeforeEach {
            $FuncParam = @{
                "Path" = "Path"
                "Keys" = @("Key1","Key2")
                "BeginProperty" = "ValidFrom"
                "EndProperty" = "ValidTo"
                "PropertyOrder" = @("Key1","Key2","ValidFrom","ValidTo","Value1","Value2")
            }
            $Csv = @(
                '"Key1","Key2","ValidFrom","ValidTo","Value1","Value2"'
                '"Key1","Key2","0","99991231","Value1","Value2"'
                '"Key3","Key4","0","99991231","Value1","Value2"'
            )
            $Records = @(@(
                @{"Key1" = "Key1"; "Key2" = "Key2"; "ValidFrom" = "0"; "ValidTo" = "99991231"; "Value1" = "Value1"; "Value2" = "Value2" }
                @{"Key1" = "Key3"; "Key2" = "Key4"; "ValidFrom" = "0"; "ValidTo" = "99991231"; "Value1" = "Value1"; "Value2" = "Value2" }
            ) | %{ New-Object -TypeName psobject -Property $_ })
        }

        It "Throws when Path is null" {
            $FuncParam["Path"] = $Null
            {Import-KeyAndPeriodListCsv @FuncParam} | Should Throw
        }

        It "Returns null when file is empty" {
            $Path = "$TestDrive\$("Returns null when file is empty".replace(" ","-")).csv"
            "" | Out-File -FilePath $Path -Encoding Default

            $FuncParam["Path"] = $Path
            $Result = Import-KeyAndPeriodListCsv @FuncParam
            $Result | Should BeNullOrEmpty
        }

        It "Returns empty list when file content is invalid" {
            $Path = "$TestDrive\$("Throws when file content is invalid".replace(" ","-")).csv"
            @("Hello, world!", "Hello, world!") | Out-File -FilePath $Path -Encoding Default
            $FuncParam["Path"] = $Path
            $Result = Import-KeyAndPeriodListCsv @FuncParam
            $Result.Length | Should Be 0
        }

        It "Returns objects when file content is valid" {
            $Path = "$TestDrive\$("Returns objects when file content is valid".replace(" ","-")).csv"
            $Csv |
            Out-File -FilePath $Path -Encoding Default
            $FuncParam["Path"] = $Path
            $Result = Import-KeyAndPeriodListCsv @FuncParam
            $Result.Length | Should Be 2

            $Expect = $Records
            Assert-Object -Result $Result -Expect $Expect
        }

        It "Accepts Path from pipeline" {
            $Path = "$TestDrive\$("Accepts Path from pipeline".replace(" ","-")).csv"
            $Csv |
            Out-File -FilePath $Path -Encoding Default
            $FuncParam2 = @{
                "Keys" = $FuncParam["Keys"]
                "BeginProperty" = $FuncParam["BeginProperty"]
                "EndProperty" = $FuncParam["EndProperty"]
                "PropertyOrder" = $FuncParam["PropertyOrder"]
            }
            $Result = $Path | Import-KeyAndPeriodListCsv @FuncParam2
            $Result.Length | Should Be 2

            $Expect = $Records
            Assert-Object -Result $Result -Expect $Expect
        }
    }
}
