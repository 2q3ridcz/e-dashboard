$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path


Describe "Import-EDashboardDailyTimeAndAmountCsv" {
    Context "Operation verification" {
        BeforeEach {
            $Csv = @(
                '"Date","Time","Amount","Note"'
                '"2021/01/01 00:00:00","0001/01/01 06:51:00",94832,"Note"'
            )
            $DailyTimeAndAmount = New-Object -TypeName psobject -Property @{
                "Date" = [datetime]::new(2021,1,1)
                "Time" = [datetime]::new(1,1,1,6,51,0)
                "Amount" = [double]94832
                "Note" = "Note"
            }
        }
        It "Throws when Path is null" {
            {Import-EDashboardDailyTimeAndAmountCsv -Path $Null} | Should Throw
        }

        It "Returns null when file is empty" {
            $Path = "$TestDrive\$("Returns null when file is empty".replace(" ","-")).csv"
            "" | Out-File -FilePath $Path -Encoding Default
            $Result = Import-EDashboardDailyTimeAndAmountCsv -Path $Path
            $Result | Should BeNullOrEmpty
        }

        It "Throws when file content is invalid" {
            $Path = "$TestDrive\$("Throws when file content is invalid".replace(" ","-")).csv"
            @("Hello, world!", "Hello, world!") | Out-File -FilePath $Path -Encoding Default
            {Import-EDashboardDailyTimeAndAmountCsv -Path $Path} | Should Throw
        }

        It "Returns DailyTimeAndAmount object file content is valid" {
            $Path = "$TestDrive\$("Returns DailyTimeAndAmount object file content is valid".replace(" ","-")).csv"
            $Csv |
            Out-File -FilePath $Path -Encoding Default
            $Result = Import-EDashboardDailyTimeAndAmountCsv -Path $Path
            $Result.GetType().Name | Should Be "DailyTimeAndAmount"

            $Expect = $DailyTimeAndAmount
            Assert-Object -Result $Result -Expect $Expect
        }

        It "Accepts Path from pipeline" {
            $Path = "$TestDrive\$("Returns DailyTimeAndAmount object file content is valid".replace(" ","-")).csv"
            $Csv |
            Out-File -FilePath $Path -Encoding Default
            $Result = $Path | Import-EDashboardDailyTimeAndAmountCsv
            $Result.GetType().Name | Should Be "DailyTimeAndAmount"

            $Expect = $DailyTimeAndAmount
            Assert-Object -Result $Result -Expect $Expect
        }
    }
}
