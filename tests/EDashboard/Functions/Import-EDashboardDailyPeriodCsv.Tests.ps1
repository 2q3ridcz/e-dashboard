$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path


Describe "Import-EDashboardDailyPeriodCsv" {
    Context "Operation verification" {
        BeforeEach {
            $Csv = @(
                '"Date","StartTime","EndTime","Note"'
                '"2021/01/01 00:00:00","0001/01/01 06:51:00","0001/01/01 16:39:00","Note"'
            )
            $DailyPeriod = New-Object -TypeName psobject -Property @{
                "Date" = [datetime]::new(2021,1,1)
                "StartTime" = [datetime]::new(1,1,1,6,51,0)
                "EndTime" = [datetime]::new(1,1,1,16,39,0)
                "Note" = "Note"
            }
        }
        It "Throws when Path is null" {
            {Import-EDashboardDailyPeriodCsv -Path $Null} | Should Throw
        }

        It "Returns null when file is empty" {
            $Path = "$TestDrive\$("Returns null when file is empty".replace(" ","-")).csv"
            "" | Out-File -FilePath $Path -Encoding Default
            $Result = Import-EDashboardDailyPeriodCsv -Path $Path
            $Result | Should BeNullOrEmpty
        }

        It "Throws when file content is invalid" {
            $Path = "$TestDrive\$("Throws when file content is invalid".replace(" ","-")).csv"
            @("Hello, world!", "Hello, world!") | Out-File -FilePath $Path -Encoding Default
            {Import-EDashboardDailyPeriodCsv -Path $Path} | Should Throw
        }

        It "Returns DailyPeriod object file content is valid" {
            $Path = "$TestDrive\$("Returns DailyPeriod object file content is valid".replace(" ","-")).csv"
            $Csv |
            Out-File -FilePath $Path -Encoding Default
            $Result = Import-EDashboardDailyPeriodCsv -Path $Path
            $Result.GetType().Name | Should Be "DailyPeriod"

            $Expect = $DailyPeriod
            Assert-Object -Result $Result -Expect $Expect
        }

        It "Accepts Path from pipeline" {
            $Path = "$TestDrive\$("Returns DailyPeriod object file content is valid".replace(" ","-")).csv"
            $Csv |
            Out-File -FilePath $Path -Encoding Default
            $Result = $Path | Import-EDashboardDailyPeriodCsv
            $Result.GetType().Name | Should Be "DailyPeriod"

            $Expect = $DailyPeriod
            Assert-Object -Result $Result -Expect $Expect
        }
    }
}
