using module ..\..\..\..\src\EDashboard\Class\repository\CsvRepository.psm1
using module ..\..\..\..\src\EDashboard\Class\entity\KeyAndPeriodListInfo.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "CsvRepository.GetterMethods" {
    BeforeEach {
        $BaseTestCase = @{
            "Path" = "Path"
            "ListInfo" = [KeyAndPeriodListInfo]::new(
                @("Key1","Key2"),
                "ValidFrom",
                "ValidTo",
                ("Key1","Key2","ValidFrom","ValidTo","Value1","Value2")
            )
        }
    }

    Context "Text" {
        $TestCases = @(
            @{"Property" = "Path"; "Value" = ""}
            @{"Property" = "Path"; "Value" = "Path"}
            @{"Property" = "Path"; "Value" = "AbCdEfG012345"}
        )
        It "Returns value when <Property> is <Value>" -TestCases $TestCases {
            param ($Property, $Value)

            $BaseTestCase[$Property] = $Value
            $obj = [CsvRepository]::new(
                $BaseTestCase["Path"],
                $BaseTestCase["ListInfo"]
            )

            $obj.GetPath() | Should Be $BaseTestCase["Path"]
            $obj.GetListInfo() | Should Be $BaseTestCase["ListInfo"]
            $obj.GetEncoding() | Should Be "Default"
        }
    }
}

Describe "CsvRepository.PathExists" {
    BeforeEach {
        $BaseTestCase = @{
            "Path" = "Path"
            "ListInfo" = [KeyAndPeriodListInfo]::new(
                @("Key1","Key2"),
                "ValidFrom",
                "ValidTo",
                ("Key1","Key2","ValidFrom","ValidTo","Value1","Value2")
            )
        }
    }

    Context "With no arguments" {
        $TestCases = @(
            @{ "TestCase" = "valid path"; "Expect" = $True}
            @{ "TestCase" = "invalid path"; "Expect" = $False}
        )
        It "Returns <Expect> to <TestCase>" -TestCases $TestCases {
            param ($TestCase, $Expect)

            $Path = "$TestDrive\somefile.csv"
            New-Item -Path $Path -ItemType File -Force

            If ($TestCase -Eq "invalid path") {
                $BaseTestCase["Path"] = "$TestDrive\abcd.csv"
            } Else {
                $BaseTestCase["Path"] = $Path
            }

            $Repo = [CsvRepository]::new($BaseTestCase["Path"],$BaseTestCase["ListInfo"])
            $Result = $Repo.PathExists()
            $Result | Should Be $Expect
        }
    }

    Context "With argument ThrowOnFalse" {
        BeforeEach {
            $Path = "$TestDrive\somefile.csv"
            New-Item -Path $Path -ItemType File -Force
        }

        $TestCases = @(
            @{ "TestCase" = "valid path"; "ThrowOnFalse" = $True; "Expect" = $True }
            @{ "TestCase" = "valid path"; "ThrowOnFalse" = $False; "Expect" = $True }
            @{ "TestCase" = "invalid path"; "ThrowOnFalse" = $False; "Expect" = $False }
        )
        It "Returns <Expect> to <TestCase> when ThrowOnFalse is <ThrowOnFalse>" -TestCases $TestCases {
            param ($TestCase, $Expect)

            If ($TestCase -Eq "invalid path") {
                $BaseTestCase["Path"] = "$TestDrive\abcd.csv"
            } Else {
                $BaseTestCase["Path"] = $Path
            }

            $Repo = [CsvRepository]::new($BaseTestCase["Path"],$BaseTestCase["ListInfo"])
            $Result = $Repo.PathExists($False)
            $Result | Should Be $Expect
        }

        $TestCases = @(
            @{ "TestCase" = "invalid path"; "ThrowOnFalse" = $True; }
        )
        It "Throws to <TestCase> when ThrowOnFalse is <ThrowOnFalse>" -TestCases $TestCases {
            param ($TestCase, $ThrowOnFalse)

            If ($TestCase -Eq "invalid path") {
                $BaseTestCase["Path"] = "$TestDrive\abcd.csv"
            } Else {
                $BaseTestCase["Path"] = $Path
            }

            $Repo = [CsvRepository]::new($BaseTestCase["Path"],$BaseTestCase["ListInfo"])

            { $Repo.PathExists($ThrowOnFalse) } | Should Throw
        }
    }
}

Describe "CsvRepository.ReadRecord" {
    BeforeEach {
        $BaseTestCase = @{
            "Path" = "Path"
            "ListInfo" = [KeyAndPeriodListInfo]::new(
                @("Key1","Key2"),
                "ValidFrom",
                "ValidTo",
                ("Key1","Key2","ValidFrom","ValidTo","Value1","Value2")
            )
        }
    }

    Context "With no arguments and input file does not exist" {
        It "Throws when file does not exist" {
            $Path = "$TestDrive\sample.csv"
            $Repo = [CsvRepository]::new($Path,$BaseTestCase["ListInfo"])
            { $Repo.ReadRecord() } | Should Throw
        }
    }

    Context "With no arguments" {
        $TestCases = @(
            @{
                "TestCase" = "0"
                "Csv" = @()
                "Expect" = @()
            }
            @{
                "TestCase" = "0"
                "Csv" = @('"Key1","Key2","ValidFrom","ValidTo","Value1","Value2"')
                "Expect" = @()
            }
            @{
                "TestCase" = "1"
                "Csv" = @(
                    '"Key1","Key2","ValidFrom","ValidTo","Value1","Value2"'
                    '"Key1","Key2",0,99991231,"Value1","Value2"'
                )
                "Expect" = @(@(
                    @{"Key1" = "Key1"; "Key2" = "Key2"; "ValidFrom" = "0"; "ValidTo" = "99991231"; "Value1" = "Value1"; "Value2" = "Value2" }
                ) | %{ New-Object -TypeName psobject -Property $_ })
            }
            @{
                "TestCase" = "2"
                "Csv" = @(
                    '"Key1","Key2","ValidFrom","ValidTo","Value1","Value2"'
                    '"Key1","Key2",0,99991231,"Value1","Value2"'
                    '"Key3","Key4",0,99991231,"Value1","Value2"'
                )
                "Expect" = @(@(
                    @{"Key1" = "Key1"; "Key2" = "Key2"; "ValidFrom" = "0"; "ValidTo" = "99991231"; "Value1" = "Value1"; "Value2" = "Value2" }
                    @{"Key1" = "Key3"; "Key2" = "Key4"; "ValidFrom" = "0"; "ValidTo" = "99991231"; "Value1" = "Value1"; "Value2" = "Value2" }
                ) | %{ New-Object -TypeName psobject -Property $_ })
            }
        )
        It "Reads <TestCase> records from csv" -TestCases $TestCases {
            param ($TestCase, $Csv, $Expect)

            $Path = "$TestDrive\$TestCase.csv"

            $Repo = [CsvRepository]::new($Path,$BaseTestCase["ListInfo"])
            $Csv | Out-File -FilePath $Path -Encoding $Repo.GetEncoding()
            $List = $Repo.ReadRecord()
            $Result = $List.Get()

            Assert-Object -Result $Result -Expect $Expect
        }
    }
}
Describe "CsvRepository.WriteRecord" {
    BeforeEach {
        $BaseTestCase = @{
            "Path" = "Path"
            "ListInfo" = [KeyAndPeriodListInfo]::new(
                @("Key1","Key2"),
                "ValidFrom",
                "ValidTo",
                ("Key1","Key2","ValidFrom","ValidTo","Value1","Value2")
            )
        }
    }

    Context "When input file does not exist" {
        $TestCases = @(
            @{
                "TestCase" = "0"
                "Records" = @()
                "Expect" = @()
            }
        )
        It "Does not create file given 0 object" -TestCases $TestCases {
            param ($TestCase, $Records, $Expect)

            $Path = "$TestDrive\NotExists-$TestCase.csv"

            $Repo = [CsvRepository]::new($Path,$BaseTestCase["ListInfo"])
            $Repo.WriteRecord($Records)
            $Path | Should Not Exist
        }


        $TestCases = @(
            @{
                "TestCase" = "1"
                "Records" = @((
                    @{"Key1" = "Key1"; "Key2" = "Key2"; "ValidFrom" = 0; "ValidTo" = 99991231; "Value1" = "Value1"; "Value2" = "Value2" }
                ) | %{ New-Object -TypeName psobject -Property $_ })
                "Expect" = @(
                    '"Key1","Key2","ValidFrom","ValidTo","Value1","Value2"'
                    '"Key1","Key2","0","99991231","Value1","Value2"'
                )
            }
            @{
                "TestCase" = "2"
                "Records" = @(@(
                    @{"Key1" = "Key1"; "Key2" = "Key2"; "ValidFrom" = 0; "ValidTo" = 99991231; "Value1" = "Value1"; "Value2" = "Value2" }
                    @{"Key1" = "Key3"; "Key2" = "Key4"; "ValidFrom" = 0; "ValidTo" = 99991231; "Value1" = "Value1"; "Value2" = "Value2" }
                ) | %{ New-Object -TypeName psobject -Property $_ })
                "Expect" = @(
                    '"Key1","Key2","ValidFrom","ValidTo","Value1","Value2"'
                    '"Key1","Key2","0","99991231","Value1","Value2"'
                    '"Key3","Key4","0","99991231","Value1","Value2"'
                )
            }
        )
        It "Creates csv file and write <TestCase> records" -TestCases $TestCases {
            param ($TestCase, $Records, $Expect)

            $Path = "$TestDrive\NotExists-$TestCase.csv"

            $Repo = [CsvRepository]::new($Path,$BaseTestCase["ListInfo"])
            $Repo.WriteRecord($Records)
            $Result = @(Get-Content -Path $Path -Encoding $Repo.GetEncoding())

            Assert-List -Result $Result -Expect $Expect
        }
    }

    Context "When input file exists" {
        $TestCases = @(
            @{
                "TestCase" = "0"
                "Records" = @()
                "Expect" = @()
            }
            @{
                "TestCase" = "1"
                "Csv" = @(
                    '"Key1","Key2","ValidFrom","ValidTo","Value1","Value2"'
                    '"Key3","Key4",0,99991231,"Value1","Value2"'
                )
                "Records" = @((
                    @{"Key1" = "Key1"; "Key2" = "Key2"; "ValidFrom" = 0; "ValidTo" = 99991231; "Value1" = "Value1"; "Value2" = "Value2" }
                ) | %{ New-Object -TypeName psobject -Property $_ })
                "Expect" = @(
                    '"Key1","Key2","ValidFrom","ValidTo","Value1","Value2"'
                    '"Key1","Key2","0","99991231","Value1","Value2"'
                    '"Key3","Key4","0","99991231","Value1","Value2"'
                )
            }
            @{
                "TestCase" = "2"
                "Csv" = @(
                    '"Key1","Key2","ValidFrom","ValidTo","Value1","Value2"'
                )
                "Records" = @(@(
                    @{"Key1" = "Key1"; "Key2" = "Key2"; "ValidFrom" = 0; "ValidTo" = 99991231; "Value1" = "Value1"; "Value2" = "Value2" }
                    @{"Key1" = "Key3"; "Key2" = "Key4"; "ValidFrom" = 0; "ValidTo" = 99991231; "Value1" = "Value1"; "Value2" = "Value2" }
                ) | %{ New-Object -TypeName psobject -Property $_ })
                "Expect" = @(
                    '"Key1","Key2","ValidFrom","ValidTo","Value1","Value2"'
                    '"Key1","Key2","0","99991231","Value1","Value2"'
                    '"Key3","Key4","0","99991231","Value1","Value2"'
                )
            }
        )
        It "Adds <TestCase> records to csv file" -TestCases $TestCases {
            param ($TestCase, $Csv, $Records, $Expect)

            $Path = "$TestDrive\Exists-$TestCase.csv"

            $Repo = [CsvRepository]::new($Path,$BaseTestCase["ListInfo"])
            $Csv | Out-File -FilePath $Path -Encoding $Repo.GetEncoding()
            $Repo.WriteRecord($Records)
            $Result = @(Get-Content -Path $Path -Encoding $Repo.GetEncoding())

            Assert-List -Result $Result -Expect $Expect
        }
    }
}
