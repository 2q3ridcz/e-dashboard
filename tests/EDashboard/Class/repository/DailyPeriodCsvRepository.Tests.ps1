using module ..\..\..\..\src\EDashboard\Class\repository\DailyPeriodCsvRepository.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "DailyPeriodCsvRepository.GetterMethods" {
    BeforeEach {
        $BaseTestCase = @{
            "Path" = "Path"
        }
    }

    Context "Text" {
        $TestCases = @(
            @{"Property" = "Path"; "Value" = ""}
            @{"Property" = "Path"; "Value" = "Path"}
            @{"Property" = "Path"; "Value" = "AbCdEfG012345"}
        )
        It "Returns value when <Property> is <Value>" -TestCases $TestCases {
            param ($Property, $Value)

            $BaseTestCase[$Property] = $Value
            $obj = [DailyPeriodCsvRepository]::new(
                $BaseTestCase["Path"]
            )

            $obj.GetPath() | Should Be $BaseTestCase["Path"]
        }
    }
}

Describe "DailyPeriodCsvRepository.GetEncoding" {
    Context "Unit test" {
        It "Returns value" {
            $obj = [DailyPeriodCsvRepository]::new("")
            $obj.GetEncoding() | Should Be "Default"
        }
    }
}

Describe "DailyPeriodCsvRepository.PathExists" {
    Context "With no arguments" {
        $TestCases = @(
            @{ "TestCase" = "valid path"; "Expect" = $True}
            @{ "TestCase" = "invalid path"; "Expect" = $False}
        )
        It "Returns <Expect> to <TestCase>" -TestCases $TestCases {
            param ($TestCase, $Expect)

            $Path = "$TestDrive\somefile.csv"
            New-Item -Path $Path -ItemType File -Force

            If ($TestCase -Eq "invalid path") { $Path = "$TestDrive\abcd.csv" }

            $Repo = [DailyPeriodCsvRepository]::new($Path)
            $Result = $Repo.PathExists()
            $Result | Should Be $Expect
        }
    }

    Context "With argument ThrowOnFalse" {
        BeforeEach {
            $Path = "$TestDrive\somefile.csv"
            New-Item -Path $Path -ItemType File -Force
        }

        $TestCases = @(
            @{ "TestCase" = "valid path"; "ThrowOnFalse" = $True; "Expect" = $True }
            @{ "TestCase" = "valid path"; "ThrowOnFalse" = $False; "Expect" = $True }
            @{ "TestCase" = "invalid path"; "ThrowOnFalse" = $False; "Expect" = $False }
        )
        It "Returns <Expect> to <TestCase> when ThrowOnFalse is <ThrowOnFalse>" -TestCases $TestCases {
            param ($TestCase, $Expect)

            If ($TestCase -Eq "invalid path") { $Path = "$TestDrive\abcd.csv" }

            $Repo = [DailyPeriodCsvRepository]::new($Path)
            $Result = $Repo.PathExists($False)
            $Result | Should Be $Expect
        }

        $TestCases = @(
            @{ "TestCase" = "invalid path"; "ThrowOnFalse" = $True; }
        )
        It "Throws to <TestCase> when ThrowOnFalse is <ThrowOnFalse>" -TestCases $TestCases {
            param ($TestCase, $ThrowOnFalse)

            If ($TestCase -Eq "invalid path") { $Path = "$TestDrive\abcd.csv" }

            $Repo = [DailyPeriodCsvRepository]::new($Path)

            { $Repo.PathExists($ThrowOnFalse) } | Should Throw
        }
    }
}

Describe "DailyPeriodCsvRepository.ReadRecord" {
    Context "With no arguments and input file does not exist" {
        It "Throws when file does not exist" {
            $Path = "$TestDrive\sample.csv"
            $Repo = [DailyPeriodCsvRepository]::new($Path)
            { $Repo.ReadRecord() } | Should Throw
        }
    }

    Context "With no arguments" {
        $TestCases = @(
            @{
                "TestCase" = "0"
                "Csv" = @()
                "Expect" = @()
            }
            @{
                "TestCase" = "0"
                "Csv" = @('"Date","StartTime","EndTime","Note"')
                "Expect" = @()
            }
            @{
                "TestCase" = "1"
                "Csv" = @(
                    '"Date","StartTime","EndTime","Note"'
                    '"2022/01/05 0:00:00","0001/01/01 9:30:00","0001/01/01 18:00:00","Worktime"'
                )
                "Expect" = @(@(
                    @{"Date" = [Datetime]::new(2022,1,5); "StartTime" = [Datetime]::new(1,1,1,9,30,0); "EndTime" = [Datetime]::new(1,1,1,18,0,0); "Note" = "Worktime"}
                ) | %{ New-Object -TypeName psobject -Property $_ })
            }
            @{
                "TestCase" = "2"
                "Csv" = @(
                    '"Date","StartTime","EndTime","Note"'
                    '"2022/01/05 0:00:00","0001/01/01 9:30:00","0001/01/01 18:00:00","Worktime"'
                    '"2022/01/06 0:00:00","0001/01/01 8:30:00","0001/01/01 17:00:00","Worktime"'
                )
                "Expect" = @(@(
                    @{"Date" = [Datetime]::new(2022,1,5); "StartTime" = [Datetime]::new(1,1,1,9,30,0); "EndTime" = [Datetime]::new(1,1,1,18,0,0); "Note" = "Worktime"}
                    @{"Date" = [Datetime]::new(2022,1,6); "StartTime" = [Datetime]::new(1,1,1,8,30,0); "EndTime" = [Datetime]::new(1,1,1,17,0,0); "Note" = "Worktime"}
                ) | %{ New-Object -TypeName psobject -Property $_ })
            }
        )
        It "Reads <TestCase> records from csv" -TestCases $TestCases {
            param ($TestCase, $Csv, $Expect)

            $Path = "$TestDrive\$TestCase.csv"

            $Repo = [DailyPeriodCsvRepository]::new($Path)
            $Csv | Out-File -FilePath $Path -Encoding $Repo.GetEncoding()
            $DailyPeriodList = $Repo.ReadRecord()
            $Result = $DailyPeriodList.Get()

            Assert-Object -Result $Result -Expect $Expect
        }
    }
}
Describe "DailyPeriodCsvRepository.CreateRecord" {
    Context "When input file does not exist" {
        $TestCases = @(
            @{
                "TestCase" = "0"
                "Records" = @()
                "Expect" = @()
            }
            @{
                "TestCase" = "1"
                "Records" = @((
                    @{"Date" = [Datetime]::new(2022,1,5); "StartTime" = [Datetime]::new(1,1,1,9,30,0); "EndTime" = [Datetime]::new(1,1,1,18,0,0); "Note" = "Worktime"}
                ) | %{ New-Object -TypeName psobject -Property $_ })
                "Expect" = @(
                    '"Date","StartTime","EndTime","Note"'
                    '"2022/01/05 00:00:00","0001/01/01 09:30:00","0001/01/01 18:00:00","Worktime"'
                )
            }
            @{
                "TestCase" = "2"
                "Records" = @(@(
                    @{"Date" = [Datetime]::new(2022,1,5); "StartTime" = [Datetime]::new(1,1,1,9,30,0); "EndTime" = [Datetime]::new(1,1,1,18,0,0); "Note" = "Worktime"}
                    @{"Date" = [Datetime]::new(2022,1,6); "StartTime" = [Datetime]::new(1,1,1,8,30,0); "EndTime" = [Datetime]::new(1,1,1,17,0,0); "Note" = "Worktime"}
                ) | %{ New-Object -TypeName psobject -Property $_ })
                "Expect" = @(
                    '"Date","StartTime","EndTime","Note"'
                    '"2022/01/05 00:00:00","0001/01/01 09:30:00","0001/01/01 18:00:00","Worktime"'
                    '"2022/01/06 00:00:00","0001/01/01 08:30:00","0001/01/01 17:00:00","Worktime"'
                )
            }
        )
        It "Creates csv file and write <TestCase> records" -TestCases $TestCases {
            param ($TestCase, $Records, $Expect)

            $Path = "$TestDrive\NotExists-$TestCase.csv"

            $Repo = [DailyPeriodCsvRepository]::new($Path)
            $Repo.CreateRecord($Records)
            $Result = @(Get-Content -Path $Path -Encoding $Repo.GetEncoding())

            Assert-List -Result $Result -Expect $Expect
        }
    }

    Context "When input file exists" {
        $TestCases = @(
            @{
                "TestCase" = "0"
                "Records" = @()
                "Expect" = @()
            }
            @{
                "TestCase" = "1"
                "Csv" = @(
                    '"Date","StartTime","EndTime","Note"'
                    '"2022/01/04 00:00:00","0001/01/01 09:30:00","0001/01/01 18:00:00","Worktime"'
                )
                "Records" = @((
                    @{"Date" = [Datetime]::new(2022,1,5); "StartTime" = [Datetime]::new(1,1,1,9,30,0); "EndTime" = [Datetime]::new(1,1,1,18,0,0); "Note" = "Worktime"}
                ) | %{ New-Object -TypeName psobject -Property $_ })
                "Expect" = @(
                    '"Date","StartTime","EndTime","Note"'
                    '"2022/01/04 00:00:00","0001/01/01 09:30:00","0001/01/01 18:00:00","Worktime"'
                    '"2022/01/05 00:00:00","0001/01/01 09:30:00","0001/01/01 18:00:00","Worktime"'
                )
            }
            @{
                "TestCase" = "2"
                "Csv" = @(
                    '"Date","StartTime","EndTime","Note"'
                    '"2022/01/04 00:00:00","0001/01/01 09:30:00","0001/01/01 18:00:00","Worktime"'
                    '"2022/01/05 00:00:00","0001/01/01 09:30:00","0001/01/01 18:00:00","Worktime"'
                )
                "Records" = @(@(
                    @{"Date" = [Datetime]::new(2022,1,5); "StartTime" = [Datetime]::new(1,1,1,9,30,0); "EndTime" = [Datetime]::new(1,1,1,18,0,0); "Note" = "Worktime"}
                    @{"Date" = [Datetime]::new(2022,1,6); "StartTime" = [Datetime]::new(1,1,1,8,30,0); "EndTime" = [Datetime]::new(1,1,1,17,0,0); "Note" = "Worktime"}
                    @{"Date" = [Datetime]::new(2022,1,7); "StartTime" = [Datetime]::new(1,1,1,9,0,0); "EndTime" = [Datetime]::new(1,1,1,17,30,0); "Note" = "Worktime"}
                ) | %{ New-Object -TypeName psobject -Property $_ })
                "Expect" = @(
                    '"Date","StartTime","EndTime","Note"'
                    '"2022/01/04 00:00:00","0001/01/01 09:30:00","0001/01/01 18:00:00","Worktime"'
                    '"2022/01/05 00:00:00","0001/01/01 09:30:00","0001/01/01 18:00:00","Worktime"'
                    '"2022/01/06 00:00:00","0001/01/01 08:30:00","0001/01/01 17:00:00","Worktime"'
                    '"2022/01/07 00:00:00","0001/01/01 09:00:00","0001/01/01 17:30:00","Worktime"'
                )
            }
        )
        It "Adds <TestCase> records to csv file" -TestCases $TestCases {
            param ($TestCase, $Csv, $Records, $Expect)

            $Path = "$TestDrive\Exists-$TestCase.csv"

            $Repo = [DailyPeriodCsvRepository]::new($Path)
            $Csv | Out-File -FilePath $Path -Encoding $Repo.GetEncoding()
            $Repo.CreateRecord($Records)
            $Result = @(Get-Content -Path $Path -Encoding $Repo.GetEncoding())

            Assert-List -Result $Result -Expect $Expect
        }
    }
}
