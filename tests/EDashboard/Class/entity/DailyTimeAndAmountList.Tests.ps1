using module ..\..\..\..\src\EDashboard\Class\entity\DailyTimeAndAmount.psm1
using module ..\..\..\..\src\EDashboard\Class\entity\DailyTimeAndAmountList.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

class DummyDailyTimeAndAmount: DailyTimeAndAmount {
    DummyDailyTimeAndAmount(
        [datetime] $Date
    ) : base(
        $Date,
        [datetime]::MinValue,
        12345,
        "Note"
    ) {}
}

Describe "DailyTimeAndAmountList.Add" {
    Context "Add to empty repository" {
        $TestCases = @(
            @{ "TestCase" = "Null"; "List" = $Null; "Expect" = @()}
            @{ "TestCase" = "Empty"; "List" = @(); "Expect" = @()}
            @{
                "TestCase" = "1 data array"
                "List" = @([DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1)))
                "Expect" = @([DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1)))
            }
            @{
                "TestCase" = "2 data array"
                "List" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,2,1))
                )
                "Expect" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,2,1))
                )
            }
        )
        It "Adds <TestCase> to list" -TestCases $TestCases {
            param ($List, $Expect)

            $Lst = [DailyTimeAndAmountList]::new()
            $Lst.Add($List)
            $Result = $Lst.Get()

            $Lst.Count() | Should Be $Expect.Length
            Assert-Object -Result $Result -Expect $Expect
        }
    }

    Context "Drop duplicates" {
        $TestCases = @(
            @{
                "TestCase" = "same records are in input"
                "Repo" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,2,1))
                )
                "List" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1))
                )
                "Expect" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,2,1))
                )
            }
            @{
                "TestCase" = "same records are in repository"
                "Repo" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,2,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,2,1))
                )
                "List" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1))
                )
                "Expect" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,2,1))
                )
            }
            @{
                "TestCase" = "same records are in input and repository"
                "Repo" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,2,1))
                )
                "List" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,2,1))
                )
                "Expect" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,2,1))
                )
            }
        )
        It "Drops duplicates on add when <TestCase>" -TestCases $TestCases {
            param ($Repo, $List, $Expect)

            $Lst = [DailyTimeAndAmountList]::new()
            $Lst.Add($Repo)
            $Lst.Add($List)
            $Result = $Lst.Get()

            $Lst.Count() | Should Be $Expect.Length
            Assert-Object -Result $Result -Expect $Expect
        }
    }

    Context "Sort" {
        $TestCases = @(
            @{
                "TestCase" = "Sorts on add"
                "List" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,3,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,2,1))
                )
                "Expect" = @(
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,1,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,2,1))
                    [DummyDailyTimeAndAmount]::new([datetime]::new(2020,3,1))
                )
            }
        )
        It "<TestCase>" -TestCases $TestCases {
            param ($List, $Expect)

            $Lst = [DailyTimeAndAmountList]::new()
            $Lst.Add($List)
            $Result = $Lst.Get()

            $Lst.Count() | Should Be $Expect.Length
            Assert-Object -Result $Result -Expect $Expect
        }
    }
}
