using module ..\..\..\..\src\EDashboard\Class\entity\DailyPeriod.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "DailyPeriod.Properties" {
    Context "Unit test" {
        It "Returns properties" {
            $Expect = @("Date","StartTime","EndTime","Note")
            $Result = [DailyPeriod]::Properties()
            Assert-List -Result $Result -Expect $Expect
        }
    }
}

Describe "DailyPeriod.GetterMethods" {
    BeforeEach {
        $BaseTestCase = @{
            "Date" = [datetime]::new(1999,1,1)
            "StartTime" = [datetime]::new(1999,1,2)
            "EndTime" = [datetime]::new(1999,1,3)
            "Note" = "Note"
        }
    }
    Context "DateProperties" {
        $TestCases = @(
            @{"Property" = "Date"; "Datetime" = [datetime]::MinValue}
            @{"Property" = "Date"; "Datetime" = [datetime]::new(2020,1,1)}
            @{"Property" = "Date"; "Datetime" = [datetime]::MaxValue}
            @{"Property" = "StartTime"; "Datetime" = [datetime]::MinValue}
            @{"Property" = "StartTime"; "Datetime" = [datetime]::new(2020,1,1)}
            @{"Property" = "StartTime"; "Datetime" = [datetime]::MaxValue}
            @{"Property" = "EndTime"; "Datetime" = [datetime]::MinValue}
            @{"Property" = "EndTime"; "Datetime" = [datetime]::new(2020,1,1)}
            @{"Property" = "EndTime"; "Datetime" = [datetime]::MaxValue}
        )
        It "Returns value when <Property> is <Datetime>" -TestCases $TestCases {
            param ($Property, $Datetime)

            $BaseTestCase[$Property] = $Datetime
            $obj = [DailyPeriod]::new(
                $BaseTestCase["Date"],
                $BaseTestCase["StartTime"],
                $BaseTestCase["EndTime"],
                $BaseTestCase["Note"]
            )

            $obj.GetDate() | Should Be $BaseTestCase["Date"]
            $obj.GetStartTime() | Should Be $BaseTestCase["StartTime"]
            $obj.GetEndTime() | Should Be $BaseTestCase["EndTime"]
            $obj.GetNote() | Should Be $BaseTestCase["Note"]
        }
    }

    Context "Text" {
        $TestCases = @(
            @{"Property" = "Note"; "Value" = ""}
            @{"Property" = "Note"; "Value" = "Note"}
            @{"Property" = "Note"; "Value" = "AbCdEfG012345"}
        )
        It "Returns value when <Property> is <Value>" -TestCases $TestCases {
            param ($Property, $Value)

            $BaseTestCase[$Property] = $Value
            $obj = [DailyPeriod]::new(
                $BaseTestCase["Date"],
                $BaseTestCase["StartTime"],
                $BaseTestCase["EndTime"],
                $BaseTestCase["Note"]
            )

            $obj.GetDate() | Should Be $BaseTestCase["Date"]
            $obj.GetStartTime() | Should Be $BaseTestCase["StartTime"]
            $obj.GetEndTime() | Should Be $BaseTestCase["EndTime"]
            $obj.GetNote() | Should Be $BaseTestCase["Note"]
        }
    }
}

Describe "DailyPeriod.ToString" {
    Context "Unit test" {
        It "Returns properties" {
            $Expect = "DailyPeriod{Date=2022/01/04 00:00:00; StartTime=0001/01/01 09:30:00; EndTime=0001/01/01 18:00:00; Note=Worktime}"
            $obj = [DailyPeriod]::new(
                [datetime]::new(2022,1,4),
                [datetime]::new(1,1,1,9,30,0),
                [datetime]::new(1,1,1,18,0,0),
                "Worktime"
            )
            $Result = $obj.ToString()
            $Result | Should Be $Expect
        }
    }
}
