using module ..\..\..\..\src\EDashboard\Class\entity\DailyTimeAndAmount.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "DailyTimeAndAmount.Properties" {
    Context "Unit test" {
        It "Returns properties" {
            $Expect = @("Date","Time","Amount","Note")
            $Result = [DailyTimeAndAmount]::Properties()
            Assert-List -Result $Result -Expect $Expect
        }
    }
}

Describe "DailyTimeAndAmount.GetterMethods" {
    BeforeEach {
        $BaseTestCase = @{
            "Date" = [datetime]::new(1999,1,1)
            "Time" = [datetime]::new(1999,1,2)
            "Amount" = 123
            "Note" = "Note"
        }
    }
    Context "Properties" {
        $TestCases = @(
            @{"Property" = "Date"; "Value" = [datetime]::MinValue}
            @{"Property" = "Date"; "Value" = [datetime]::new(2020,1,1)}
            @{"Property" = "Date"; "Value" = [datetime]::MaxValue}
            @{"Property" = "Time"; "Value" = [datetime]::MinValue}
            @{"Property" = "Time"; "Value" = [datetime]::new(2020,1,1)}
            @{"Property" = "Time"; "Value" = [datetime]::MaxValue}
            @{"Property" = "Amount"; "Value" = 0}
            @{"Property" = "Amount"; "Value" = 100}
            @{"Property" = "Amount"; "Value" = 9999999999}
            @{"Property" = "Note"; "Value" = ""}
            @{"Property" = "Note"; "Value" = "Note"}
            @{"Property" = "Note"; "Value" = "AbCdEfG012345"}
        )
        It "Returns value when <Property> is <Value>" -TestCases $TestCases {
            param ($Property, $Value)

            $BaseTestCase[$Property] = $Value
            $obj = [DailyTimeAndAmount]::new(
                $BaseTestCase["Date"],
                $BaseTestCase["Time"],
                $BaseTestCase["Amount"],
                $BaseTestCase["Note"]
            )

            $obj.GetDate() | Should Be $BaseTestCase["Date"]
            $obj.GetTime() | Should Be $BaseTestCase["Time"]
            $obj.GetAmount() | Should Be $BaseTestCase["Amount"]
            $obj.GetNote() | Should Be $BaseTestCase["Note"]
        }
    }
}

Describe "DailyTimeAndAmount.ToString" {
    Context "Unit test" {
        It "Returns properties" {
            $Expect = "DailyTimeAndAmount{Date=2022/01/04 00:00:00; Time=0001/01/01 09:30:00; Amount=123; Note=Worktime}"
            $obj = [DailyTimeAndAmount]::new(
                [datetime]::new(2022,1,4),
                [datetime]::new(1,1,1,9,30,0),
                123,
                "Worktime"
            )
            $Result = $obj.ToString()
            $Result | Should Be $Expect
        }
    }
}
