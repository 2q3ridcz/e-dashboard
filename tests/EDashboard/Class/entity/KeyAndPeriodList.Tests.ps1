using module ..\..\..\..\src\EDashboard\Class\entity\KeyAndPeriodList.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "KeyAndPeriodList.Constructor" {
    Context "Unit test" {
        BeforeEach {
            $BaseTestCase = @{
                "Keys" = @("Key1","Key2")
                "BeginProperty" = "ValidFrom"
                "EndProperty" = "ValidTo"
                "PropertyOrder" = @("Key1","Key2","ValidFrom","ValidTo","Value1","Value2")
            }
        }

        It "Creates object" { 
            $obj = [KeyAndPeriodList]::new(
                $BaseTestCase["Keys"],
                $BaseTestCase["BeginProperty"],
                $BaseTestCase["EndProperty"],
                $BaseTestCase["PropertyOrder"]
            )
            Assert-List -Result $obj.GetKeys() -Expect $BaseTestCase["Keys"]
            $obj.GetBeginProperty() | Should Be $BaseTestCase["BeginProperty"]
            $obj.GetEndProperty() | Should Be $BaseTestCase["EndProperty"]
            Assert-List -Result $obj.GetPropertyOrder() -Expect $BaseTestCase["PropertyOrder"]
        }


        $TestCases = @(
            @{"Property" = "Keys"; "Value" = @()}
            @{"Property" = "Keys"; "Value" = @("ValidFrom")}
            @{"Property" = "Keys"; "Value" = @("ValidTo")}
            @{"Property" = "BeginProperty"; "Value" = ""}
            @{"Property" = "EndProperty"; "Value" = ""}
            @{"Property" = "EndProperty"; "Value" = "ValidFrom"}
            @{"Property" = "PropertyOrder"; "Value" = @()}
            @{"Property" = "PropertyOrder"; "Value" = @("Key1","ValidFrom","ValidTo","Value1","Value2")}
            @{"Property" = "PropertyOrder"; "Value" = @("Key1","Key2","ValidTo","Value1","Value2")}
            @{"Property" = "PropertyOrder"; "Value" = @("Key1","Key2","ValidFrom","Value1","Value2")}
        )
        It "Throws when <Property> is <Value>" -TestCases $TestCases {
            param ($Property, $Value)
            $BaseTestCase[$Property] = $Value
            {
                [KeyAndPeriodList]::new(
                    $BaseTestCase["Keys"],
                    $BaseTestCase["BeginProperty"],
                    $BaseTestCase["EndProperty"],
                    $BaseTestCase["PropertyOrder"]
                )
            } | Should Throw
        }
    }
}

Describe "KeyAndPeriodList.Add" {
    Context "Objects to ignore" {
        BeforeAll {
            $BaseTestCase = @{
                "Keys" = @("Key1","Key2")
                "BeginProperty" = "ValidFrom"
                "EndProperty" = "ValidTo"
                "PropertyOrder" = @("Key1","Key2","ValidFrom","ValidTo","Value1","Value2")
            }
        }

        It "Ignores objects without BeginProperty" {
            $Lst = [KeyAndPeriodList]::new(
                $BaseTestCase["Keys"],
                $BaseTestCase["BeginProperty"],
                $BaseTestCase["EndProperty"],
                $BaseTestCase["PropertyOrder"]
            )
            $Obj = @(
                "Key1,Key2,ValidTo,Value1,Value2
                key1-1,key2-1,20220331,Value1-20220101,Value2-20220331
                key1-1,key2-1,20220630,Value1-20220401,Value2-20220630
                key1-1,key2-1,99991231,Value1-20220701,Value2-99991231
                " | ConvertFrom-Csv
            )
            $Lst.Add($Obj)
            $Lst.Count() | Should Be 0
        }
        It "Ignores objects without EndProperty" {
            $Lst = [KeyAndPeriodList]::new(
                $BaseTestCase["Keys"],
                $BaseTestCase["BeginProperty"],
                $BaseTestCase["EndProperty"],
                $BaseTestCase["PropertyOrder"]
            )
            $Obj = @(
                "Key1,Key2,ValidFrom,Value1,Value2
                key1-1,key2-1,20220101,Value1-20220101,Value2-20220331
                key1-1,key2-1,20220401,Value1-20220401,Value2-20220630
                key1-1,key2-1,20220701,Value1-20220701,Value2-99991231
                " | ConvertFrom-Csv
            )
            $Lst.Add($Obj)
            $Lst.Count() | Should Be 0
        }
    }

    Context "One key list" {
        BeforeAll {
            $BaseTestCase = @{
                "Keys" = @("Key1","Key2")
                "BeginProperty" = "ValidFrom"
                "EndProperty" = "ValidTo"
                "PropertyOrder" = @("Key1","Key2","ValidFrom","ValidTo","Value1","Value2")
            }
            $EmptyObj = @()
            $BeforeObj = @(
                "Key1,Key2,ValidFrom,ValidTo,Value1,Value2
                key1-1,key2-1,20220101,20220331,Value1-20220101,Value2-20220331
                key1-1,key2-1,20220401,20220630,Value1-20220401,Value2-20220630
                key1-1,key2-1,20220701,99991231,Value1-20220701,Value2-99991231
                " | ConvertFrom-Csv
            )
            $AddObjFromBegin = @(
                "Key1,Key2,ValidFrom,ValidTo,Value1,Value2
                key1-1,key2-1,20220401,20220531,Value1-20220401,Value2-20220531
                " | ConvertFrom-Csv
            )
            $ExpectObjFromBegin = @(
                "Key1,Key2,ValidFrom,ValidTo,Value1,Value2
                key1-1,key2-1,20220101,20220331,Value1-20220101,Value2-20220331
                key1-1,key2-1,20220401,20220531,Value1-20220401,Value2-20220531
                " | ConvertFrom-Csv
            )
            $AddObjFromBetweenBeginAndEnd = @(
                "Key1,Key2,ValidFrom,ValidTo,Value1,Value2
                key1-1,key2-1,20220501,20220630,Value1-20220501,Value2-20220630
                " | ConvertFrom-Csv
            )
            $ExpectObjFromBetweenBeginAndEnd = @(
                "Key1,Key2,ValidFrom,ValidTo,Value1,Value2
                key1-1,key2-1,20220101,20220331,Value1-20220101,Value2-20220331
                key1-1,key2-1,20220401,20220500,Value1-20220401,Value2-20220630
                key1-1,key2-1,20220501,20220630,Value1-20220501,Value2-20220630
                " | ConvertFrom-Csv
            )
            $AddObjFromEnd = @(
                "Key1,Key2,ValidFrom,ValidTo,Value1,Value2
                key1-1,key2-1,20220630,20220731,Value1-20220630,Value2-20220731
                key1-1,key2-1,20220801,99991231,Value1-20220801,Value2-99991231
                " | ConvertFrom-Csv
            )
            $ExpectObjFromEnd = @(
                "Key1,Key2,ValidFrom,ValidTo,Value1,Value2
                key1-1,key2-1,20220101,20220331,Value1-20220101,Value2-20220331
                key1-1,key2-1,20220401,20220629,Value1-20220401,Value2-20220630
                key1-1,key2-1,20220630,20220731,Value1-20220630,Value2-20220731
                key1-1,key2-1,20220801,99991231,Value1-20220801,Value2-99991231
                " | ConvertFrom-Csv
            )
        }
        $TestCases = @(
            @{"TestCase" = "Adds to empty"; "Before" = $EmptyObj; "Add" = $BeforeObj; "Expect" = $BeforeObj}
            @{"TestCase" = "Add-object's begin equals before object's begin"; "Before" = $BeforeObj; "Add" = $AddObjFromBegin; "Expect" = $ExpectObjFromBegin}
            @{"TestCase" = "Add-object's begin is between before object's begin and end"; "Before" = $BeforeObj; "Add" = $AddObjFromBetweenBeginAndEnd; "Expect" = $ExpectObjFromBetweenBeginAndEnd}
            @{"TestCase" = "Add-object's begin equals before object's end"; "Before" = $BeforeObj; "Add" = $AddObjFromEnd; "Expect" = $ExpectObjFromEnd}
        )
        It "<TestCase>" -TestCases $TestCases {
            param ($Before, $Add, $Expect)

            $Lst = [KeyAndPeriodList]::new(
                $BaseTestCase["Keys"],
                $BaseTestCase["BeginProperty"],
                $BaseTestCase["EndProperty"],
                $BaseTestCase["PropertyOrder"]
            )

            $Lst.Add($Before)
            $Lst.Add($Add)
            $Result = $Lst.Get()

            Assert-Object -Result $Result -Expect $Expect -VoidTypeCheck
            $Lst.Count() | Should Be $Expect.Length
        }
    }
}
