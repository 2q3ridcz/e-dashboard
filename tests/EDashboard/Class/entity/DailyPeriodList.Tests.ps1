using module ..\..\..\..\src\EDashboard\Class\entity\DailyPeriod.psm1
using module ..\..\..\..\src\EDashboard\Class\entity\DailyPeriodList.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

class DummyDailyPeriod: DailyPeriod {
    DummyDailyPeriod(
        [datetime] $Date
    ) : base(
        $Date,
        [datetime]::MinValue,
        [datetime]::MaxValue,
        "Note"
    ) {}
}

Describe "DailyPeriodList.Add" {
    Context "Add to empty repository" {
        $TestCases = @(
            @{ "TestCase" = "Null"; "List" = $Null; "Expect" = @()}
            @{ "TestCase" = "Empty"; "List" = @(); "Expect" = @()}
            @{
                "TestCase" = "1 data array"
                "List" = @([DummyDailyPeriod]::new([datetime]::new(2020,1,1)))
                "Expect" = @([DummyDailyPeriod]::new([datetime]::new(2020,1,1)))
            }
            @{
                "TestCase" = "2 data array"
                "List" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,1,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,2,1))
                )
                "Expect" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,1,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,2,1))
                )
            }
        )
        It "Adds <TestCase> to list" -TestCases $TestCases {
            param ($List, $Expect)

            $Lst = [DailyPeriodList]::new()
            $Lst.Add($List)
            $Result = $Lst.Get()

            $Lst.Count() | Should Be $Expect.Length
            Assert-Object -Result $Result -Expect $Expect
        }
    }

    Context "Drop duplicates" {
        $TestCases = @(
            @{
                "TestCase" = "same records are in input"
                "Repo" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,2,1))
                )
                "List" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,1,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,1,1))
                )
                "Expect" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,1,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,2,1))
                )
            }
            @{
                "TestCase" = "same records are in repository"
                "Repo" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,2,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,2,1))
                )
                "List" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,1,1))
                )
                "Expect" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,1,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,2,1))
                )
            }
            @{
                "TestCase" = "same records are in input and repository"
                "Repo" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,1,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,2,1))
                )
                "List" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,1,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,2,1))
                )
                "Expect" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,1,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,2,1))
                )
            }
        )
        It "Drops duplicates on add when <TestCase>" -TestCases $TestCases {
            param ($Repo, $List, $Expect)

            $Lst = [DailyPeriodList]::new()
            $Lst.Add($Repo)
            $Lst.Add($List)
            $Result = $Lst.Get()

            $Lst.Count() | Should Be $Expect.Length
            Assert-Object -Result $Result -Expect $Expect
        }
    }

    Context "Sort" {
        $TestCases = @(
            @{
                "TestCase" = "Sorts on add"
                "List" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,3,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,1,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,2,1))
                )
                "Expect" = @(
                    [DummyDailyPeriod]::new([datetime]::new(2020,1,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,2,1))
                    [DummyDailyPeriod]::new([datetime]::new(2020,3,1))
                )
            }
        )
        It "<TestCase>" -TestCases $TestCases {
            param ($List, $Expect)

            $Lst = [DailyPeriodList]::new()
            $Lst.Add($List)
            $Result = $Lst.Get()

            $Lst.Count() | Should Be $Expect.Length
            Assert-Object -Result $Result -Expect $Expect
        }
    }

    ### Deleted this overload. This causes error on Add($Null).
    ### > MethodException: Multiple ambiguous overloads found for "Add" and the argument count: "1".
    # Context "Input DailyPeriodList" {
    #     $TestCases = @(
    #         @{
    #             "TestCase" = "empty"
    #             "List" = @(
    #             )
    #             "Expect" = @(
    #             )
    #         }
    #     )
    #     It "Receives <TestCase> DailyPeriodList as input" -TestCases $TestCases {
    #         param ($List, $Expect)

    #         $InputLst = [DailyPeriodList]::new()
    #         $InputLst.Add($List)

    #         $Lst = [DailyPeriodList]::new()
    #         $Lst.Add($InputLst)
    #         $Result = $Lst.Get()

    #         $Lst.Count() | Should Be $Expect.Length
    #         Assert-Object -Result $Result -Expect $Expect
    #     }
    # }
}
