$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\..\" | Resolve-Path
$TestsFolderPath = "$here\..\" | Resolve-Path

$PackageName = "TestHelper"
$PackagePath = $TestsFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
Import-Module -Name $PackagePath -Force


$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\..\" | Resolve-Path
$TestsFolderPath = "$here\..\" | Resolve-Path

$PackageName = "EDashboard"
$PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
Import-Module -Name $PackagePath -Force
