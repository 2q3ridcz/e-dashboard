﻿using module ..\Class\entity\KeyAndPeriodListInfo.psm1
using module ..\Class\repository\CsvRepository.psm1

function Export-KeyAndPeriodListCsv {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, Position=0)]
        [object[]]
        $InputObject
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $Path
        ,
        [Parameter(Mandatory=$True)]
        [string[]]
        $Keys
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $BeginProperty
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $EndProperty
        ,
        [Parameter(Mandatory=$True)]
        [string[]]
        $PropertyOrder
    )
    begin {
        $Records = @()
    }
    process {
        $Records += $InputObject
    }
    end {
        $ListInfo = [KeyAndPeriodListInfo]::new(
            $Keys,
            $BeginProperty,
            $EndProperty,
            $PropertyOrder
        )
        $Repo = [CsvRepository]::new($Path, $ListInfo)
        $Repo.WriteRecord($Records)
    }
}
