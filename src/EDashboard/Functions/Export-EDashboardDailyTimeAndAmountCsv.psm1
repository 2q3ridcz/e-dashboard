﻿using module ..\Class\repository\DailyTimeAndAmountCsvRepository.psm1

function Export-EDashboardDailyTimeAndAmountCsv {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, Position=0)]
        [object[]]
        $InputObject
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $Path
    )
    begin {
        $Records = @()
    }
    process {
        $Records += $InputObject
    }
    end {
        $Repo = [DailyTimeAndAmountCsvRepository]::new($Path)
        $Repo.CreateRecord($Records)
    }
}
