﻿using module ..\Class\repository\DailyPeriodCsvRepository.psm1

function Export-EDashboardDailyPeriodCsv {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, Position=0)]
        [object[]]
        $InputObject
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $Path
    )
    begin {
        $Records = @()
    }
    process {
        $Records += $InputObject
    }
    end {
        $Repo = [DailyPeriodCsvRepository]::new($Path)
        $Repo.CreateRecord($Records)
    }
}
