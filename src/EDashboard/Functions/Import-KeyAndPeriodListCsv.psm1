﻿using module ..\Class\entity\KeyAndPeriodListInfo.psm1
using module ..\Class\repository\CsvRepository.psm1

function Import-KeyAndPeriodListCsv {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, Position=0)]
        [string]
        $Path
        ,
        [Parameter(Mandatory=$True)]
        [string[]]
        $Keys
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $BeginProperty
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $EndProperty
        ,
        [Parameter(Mandatory=$True)]
        [string[]]
        $PropertyOrder
    )
    begin {
        $ListInfo = [KeyAndPeriodListInfo]::new(
            $Keys,
            $BeginProperty,
            $EndProperty,
            $PropertyOrder
        )
    }
    process {
        $Repo = [CsvRepository]::new($Path, $ListInfo)
        $List = $Repo.ReadRecord()
        $List.Get()
    }
    end {}
}
