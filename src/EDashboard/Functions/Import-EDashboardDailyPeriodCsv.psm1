﻿using module ..\Class\repository\DailyPeriodCsvRepository.psm1

function Import-EDashboardDailyPeriodCsv {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, Position=0)]
        [string]
        $Path
    )
    begin {}
    process {
        $Repo = [DailyPeriodCsvRepository]::new($Path)
        $DailyPeriodList = $Repo.ReadRecord()
        $DailyPeriodList.Get()
    }
    end {}
}
