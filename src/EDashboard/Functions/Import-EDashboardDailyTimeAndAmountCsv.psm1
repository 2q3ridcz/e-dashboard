﻿using module ..\Class\repository\DailyTimeAndAmountCsvRepository.psm1

function Import-EDashboardDailyTimeAndAmountCsv {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, Position=0)]
        [string]
        $Path
    )
    begin {}
    process {
        $Repo = [DailyTimeAndAmountCsvRepository]::new($Path)
        $DailyTimeAndAmountList = $Repo.ReadRecord()
        $DailyTimeAndAmountList.Get()
    }
    end {}
}
