using module .\DailyTimeAndAmount.psm1

class DailyTimeAndAmountList {
    [DailyTimeAndAmount[]] $List = @()

    DailyTimeAndAmountList() {}

    [DailyTimeAndAmount[]] Get() { return $this.List }
    [int] Count() { return $this.Get().Length }

    [void] Add([DailyTimeAndAmount[]] $DailyTimeAndAmount) {
        $Lst = $this.Get()

        If (1 -Le $DailyTimeAndAmount.Length) {
            $this.List =
            ($Lst + $DailyTimeAndAmount) |
            Sort-Object -Unique -Property ([DailyTimeAndAmount]::Properties())
        }
    }
}
