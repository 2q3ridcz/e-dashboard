using module .\KeyAndPeriodList.psm1

class KeyAndPeriodListInfo {
    [string[]] $Keys
    [string] $BeginProperty
    [string] $EndProperty
    [string[]] $PropertyOrder

    KeyAndPeriodListInfo(
        [string[]] $Keys,
        [string] $BeginProperty,
        [string] $EndProperty,
        [string[]] $PropertyOrder
    ) {
        $this.Keys = $Keys
        $this.BeginProperty = $BeginProperty
        $this.EndProperty = $EndProperty
        $this.PropertyOrder = $PropertyOrder
    }

    [string[]] GetKeys() { return $this.Keys }
    [string] GetBeginProperty() { return $this.BeginProperty }
    [string] GetEndProperty() { return $this.EndProperty }
    [string[]] GetPropertyOrder() { return $this.PropertyOrder }

    [object] NewList() {
        $List = [KeyAndPeriodList]::new(
            $this.GetKeys(),
            $this.GetBeginProperty(),
            $this.GetEndProperty(),
            $this.GetPropertyOrder()
        )
        return $List
    }
}
