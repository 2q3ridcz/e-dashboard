class DailyPeriod {
    [datetime] $Date
    [datetime] $StartTime
    [datetime] $EndTime
    [string] $Note

    DailyPeriod(
        [datetime] $Date,
        [datetime] $StartTime,
        [datetime] $EndTime,
        [string] $Note
    ) {
        $this.Date = $Date
        $this.StartTime = $StartTime
        $this.EndTime = $EndTime
        $this.Note = $Note
    }

    static [string[]] Properties() { return @("Date","StartTime","EndTime","Note")}

    [datetime] GetDate() { return $this.Date }
    [datetime] GetStartTime() { return $this.StartTime }
    [datetime] GetEndTime() { return $this.EndTime }
    [string] GetNote() { return $this.Note }

    [string] ToString() {
        $PropertyDict = @{}
        @("Date","StartTime","EndTime") | %{ $PropertyDict[$_] = $this.($_).ToString("yyyy/MM/dd HH:mm:ss") }
        @("Note") | %{ $PropertyDict[$_] = $this.($_) }

        return @(
            "DailyPeriod{"
            @([DailyPeriod]::Properties() | %{ @($_, $PropertyDict[$_]) -Join "=" }) -Join "; "
            "}"
        ) -Join ""
    }
}
