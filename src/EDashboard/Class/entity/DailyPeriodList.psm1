using module .\DailyPeriod.psm1

class DailyPeriodList {
    [DailyPeriod[]] $List = @()

    DailyPeriodList() {}

    [DailyPeriod[]] Get() { return $this.List }
    [int] Count() { return $this.Get().Length }

    ### Deleted this overload. This causes error on Add($Null).
    ### > MethodException: Multiple ambiguous overloads found for "Add" and the argument count: "1".
    # [void] Add([DailyPeriodList] $DailyPeriodList) { $this.Add($DailyPeriodList.Get())}
    [void] Add([DailyPeriod[]] $DailyPeriod) {
        $Lst = $this.Get()

        If (1 -Le $DailyPeriod.Length) {
            $this.List =
            ($Lst + $DailyPeriod) |
            Sort-Object -Unique -Property ([DailyPeriod]::Properties())
        }
    }
}
