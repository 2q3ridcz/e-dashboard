class DailyTimeAndAmount {
    [datetime] $Date
    [datetime] $Time
    [double] $Amount
    [string] $Note

    DailyTimeAndAmount(
        [datetime] $Date,
        [datetime] $Time,
        [double] $Amount,
        [string] $Note
    ) {
        $this.Date = $Date
        $this.Time = $Time
        $this.Amount = $Amount
        $this.Note = $Note
    }

    static [string[]] Properties() { return @("Date","Time","Amount","Note")}

    [datetime] GetDate() { return $this.Date }
    [datetime] GetTime() { return $this.Time }
    [double] GetAmount() { return $this.Amount }
    [string] GetNote() { return $this.Note }

    [string] ToString() {
        $PropertyDict = @{}
        @("Date","Time") | %{ $PropertyDict[$_] = $this.($_).ToString("yyyy/MM/dd HH:mm:ss") }
        @("Amount","Note") | %{ $PropertyDict[$_] = $this.($_) }

        return @(
            "DailyTimeAndAmount{"
            @([DailyTimeAndAmount]::Properties() | %{ @($_, $PropertyDict[$_]) -Join "=" }) -Join "; "
            "}"
        ) -Join ""
    }
}
