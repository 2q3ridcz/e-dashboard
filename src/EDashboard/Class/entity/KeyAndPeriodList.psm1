class KeyAndPeriodList {
    [object[]] $List = @()
    [string[]] $Keys
    [string] $BeginProperty
    [string] $EndProperty
    [string[]] $PropertyOrder

    KeyAndPeriodList(
        [string[]] $Keys,
        [string] $BeginProperty,
        [string] $EndProperty,
        [string[]] $PropertyOrder
    ) {
        If ($Keys.Length -Eq 0) { Throw "Keys should have one or more values" }
        If ($Keys -Contains $BeginProperty) { Throw "Keys should not contain BeginProperty" }
        If ($Keys -Contains $EndProperty) { Throw "Keys should not contain EndProperty" }
        If ($BeginProperty -Eq $EndProperty) { Throw "BeginProperty should not be the same as EndProperty" }
        If ($Null -Ne ($Keys | ?{$PropertyOrder -NotContains $_})) { Throw "PropertyOrder should contain Keys" }
        If ($PropertyOrder -NotContains $BeginProperty) { Throw "PropertyOrder should contain BeginProperty" }
        If ($PropertyOrder -NotContains $EndProperty) { Throw "PropertyOrder should contain EndProperty" }
        $this.Keys = $Keys
        $this.BeginProperty = $BeginProperty
        $this.EndProperty = $EndProperty
        $this.PropertyOrder = $PropertyOrder
    }

    [string[]] GetKeys() { return $this.Keys }
    [string] GetBeginProperty() { return $this.BeginProperty }
    [string] GetEndProperty() { return $this.EndProperty }
    [string[]] GetPropertyOrder() { return $this.PropertyOrder }

    [object[]] Get() { return $this.List }
    [int] Count() { return $this.Get().Length }

    [void] Add([object[]] $Object) {
        # Object's BeginProperty and EndProperty value must be int.

        If ($Object.Length -Eq 0) { return }
        $Lst = $this.Get()
        $KeyList = $this.GetKeys()
        $BeginProp = $this.GetBeginProperty()
        $EndProp = $this.GetEndProperty()
        $PropOrder = $this.GetPropertyOrder()

        $AddMemberName = "__AddOrder"

        $TempObject = $Object | Select-Object -Property ($PropOrder + $AddMemberName) |
        %{
            $_.$AddMemberName = 1
            foreach ($Key in $KeyList) {
                ### Avoid unexpected key unmatch that may occur due to $Object's $Null.
                ### This is because $Null is converted to ""(blank) when exported to csv.
                If ($Null -Eq $_.$Key) {$_.$Key = ""}
            }
            $_
        }

        $NewList =
        @(
            $Lst | Add-Member -MemberType NoteProperty -Name $AddMemberName -Value 0 -PassThru
            $TempObject
        ) |
        ?{ $Null -Ne $_.$BeginProp } |
        ?{ $Null -Ne $_.$EndProp } |
        Group-Object -Property $KeyList |
        %{
            $BeginMin = 100000000

            $_.Group | Group-Object -Property $AddMemberName | Sort-Object -Property "Name" -Descending |
            %{
                $_.Group |
                %{
                    $Begin = [int]($_.$BeginProp)
                    $End = [int]($_.$EndProp)

                    If ($End -Lt $BeginMin) {
                        # Add
                        $_ | Write-Output
                    } ElseIf ($BeginMin -Le $Begin) {
                        # Delete
                    } Else {
                        # Change
                        $_.$EndProp = $BeginMin - 1
                        $_ | Write-Output
                    }
                }
                $TempBeginMin = $_.Group | Measure-Object -Property $BeginProp -Minimum | %{[int]($_.Minimum)}
                If ($TempBeginMin -Lt $BeginMin) { $BeginMin = $TempBeginMin }
            }
        } |
        Sort-Object -Unique -Property $PropOrder |
        Select-Object -Property $PropOrder

        $this.List = $NewList
    }
}
