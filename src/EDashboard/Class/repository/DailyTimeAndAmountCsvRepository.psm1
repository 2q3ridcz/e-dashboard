using module ..\entity\DailyTimeAndAmount.psm1
using module ..\entity\DailyTimeAndAmountList.psm1

class DailyTimeAndAmountCsvRepository {
    [string] $Path

    DailyTimeAndAmountCsvRepository(
        [string] $Path
    ) {
        $this.Path = $Path
    }

    [string] GetPath() { return $this.Path }
    [string] GetEncoding() { return "Default" }

    [bool] PathExists() { return $this.PathExists($False) }
    [bool] PathExists([bool] $ThrowOnFalse) {
        $CsvPath = $this.GetPath()
        $PathExists = $CsvPath | Test-Path -PathType Leaf
        If ($ThrowOnFalse) {
            If (-Not $PathExists) {
                throw ("DailyTimeAndAmountCsvRepository: Path does not exist. Path: " + $CsvPath)
            }
        }
        return $PathExists
    }

    [DailyTimeAndAmountList] ReadRecord() {
        $this.PathExists($True)
        $CsvPath = $this.GetPath()
        $Enc = $this.GetEncoding()

        $ObjList =
        Import-Csv -Path $CsvPath -Encoding $Enc |
        %{
            [DailyTimeAndAmount]::new(
                [Datetime]::Parse($_.Date),
                [Datetime]::Parse($_.Time),
                $_.Amount,
                $_.Note
            )
        }
        $Lst = [DailyTimeAndAmountList]::new()
        $Lst.Add($ObjList)
        return $Lst
    }

    [void] CreateRecord([Object[]] $DailyTimeAndAmount) {
        $PathExists = $this.PathExists()
        $CsvPath = $this.GetPath()
        $Enc = $this.GetEncoding()

        $ColumnName = [DailyTimeAndAmount]::Properties()

        $ObjList =
        $DailyTimeAndAmount |
        %{
            [DailyTimeAndAmount]::new(
                [Datetime]::Parse($_.Date),
                [Datetime]::Parse($_.Time),
                $_.Amount,
                $_.Note
            )
        }
        $Lst = [DailyTimeAndAmountList]::new()
        $Lst.Add($ObjList)

        If ( $PathExists ) {
            $Lst.Add($this.ReadRecord().Get())
        }

        $CsvObjList =
        $Lst.Get() |
        %{
            New-Object -TypeName psobject -Property @{
                "Date" = $_.Date.ToString("yyyy/MM/dd HH:mm:ss")
                "Time" = $_.Time.ToString("yyyy/MM/dd HH:mm:ss")
                "Amount" = $_.Amount
                "Note" = $_.Note
            }
        }

        $CsvObjList |
        Select-Object -Property $ColumnName |
        Export-Csv -Path $CsvPath -Encoding $Enc -NoTypeInformation
    }
}
