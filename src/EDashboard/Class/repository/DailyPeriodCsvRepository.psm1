using module ..\entity\DailyPeriod.psm1
using module ..\entity\DailyPeriodList.psm1

class DailyPeriodCsvRepository {
    [string] $Path

    DailyPeriodCsvRepository(
        [string] $Path
    ) {
        $this.Path = $Path
    }

    [string] GetPath() { return $this.Path }
    [string] GetEncoding() { return "Default" }

    [bool] PathExists() { return $this.PathExists($False) }
    [bool] PathExists([bool] $ThrowOnFalse) {
        $CsvPath = $this.GetPath()
        $PathExists = $CsvPath | Test-Path -PathType Leaf
        If ($ThrowOnFalse) {
            If (-Not $PathExists) {
                throw ("DailyPeriodCsvRepository: Path does not exist. Path: " + $CsvPath)
            }
        }
        return $PathExists
    }

    [DailyPeriodList] ReadRecord() {
        $this.PathExists($True)
        $CsvPath = $this.GetPath()
        $Enc = $this.GetEncoding()

        $ObjList =
        Import-Csv -Path $CsvPath -Encoding $Enc |
        %{
            [DailyPeriod]::new(
                [Datetime]::Parse($_.Date),
                [Datetime]::Parse($_.StartTime),
                [Datetime]::Parse($_.EndTime),
                $_.Note
            )
        }
        $Lst = [DailyPeriodList]::new()
        $Lst.Add($ObjList)
        return $Lst
    }

    [void] CreateRecord([Object[]] $DailyPeriod) {
        $PathExists = $this.PathExists()
        $CsvPath = $this.GetPath()
        $Enc = $this.GetEncoding()

        $ColumnName = [DailyPeriod]::Properties()

        $ObjList =
        $DailyPeriod |
        %{
            [DailyPeriod]::new(
                [Datetime]::Parse($_.Date),
                [Datetime]::Parse($_.StartTime),
                [Datetime]::Parse($_.EndTime),
                $_.Note
            )
        }
        $Lst = [DailyPeriodList]::new()
        $Lst.Add($ObjList)

        If ( $PathExists ) {
            $Lst.Add($this.ReadRecord().Get())
        }

        $CsvObjList =
        $Lst.Get() |
        %{
            New-Object -TypeName psobject -Property @{
                "Date" = $_.Date.ToString("yyyy/MM/dd HH:mm:ss")
                "StartTime" = $_.StartTime.ToString("yyyy/MM/dd HH:mm:ss")
                "EndTime" = $_.EndTime.ToString("yyyy/MM/dd HH:mm:ss")
                "Note" = $_.Note
            }
        }

        $CsvObjList |
        Select-Object -Property $ColumnName |
        Export-Csv -Path $CsvPath -Encoding $Enc -NoTypeInformation
    }
}
