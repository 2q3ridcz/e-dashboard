class CsvRepository {
    [string] $Path
    [object] $ListInfo

    CsvRepository(
        [string] $Path,
        [object] $ListInfo
    ) {
        $this.Path = $Path
        $this.ListInfo = $ListInfo
    }

    [string] GetPath() { return $this.Path }
    [object] GetListInfo() { return $this.ListInfo }
    [string] GetEncoding() { return "Default" }

    [bool] PathExists() { return $this.PathExists($False) }
    [bool] PathExists([bool] $ThrowOnFalse) {
        $CsvPath = $this.GetPath()
        $PathExists = $CsvPath | Test-Path -PathType Leaf
        If ($ThrowOnFalse) {
            If (-Not $PathExists) {
                throw ("CsvRepository: Path does not exist. Path: " + $CsvPath)
            }
        }
        return $PathExists
    }

    [object] ReadRecord() {
        $this.PathExists($True)
        $CsvPath = $this.GetPath()
        $LstInfo = $this.GetListInfo()
        $Enc = $this.GetEncoding()

        $ObjList = @(Import-Csv -Path $CsvPath -Encoding $Enc)
        $Lst = $LstInfo.NewList()
        $Lst.Add($ObjList)
        return $Lst
    }

    [void] WriteRecord([object[]] $Object) {
        If ($Object.Length -Eq 0) { return }

        $PathExists = $this.PathExists()
        $CsvPath = $this.GetPath()
        $LstInfo = $this.GetListInfo()
        $Enc = $this.GetEncoding()

        If ( $PathExists ) {
            $Lst = $this.ReadRecord()
        } Else {
            $Lst = $LstInfo.NewList()
        }
        $Lst.Add($Object)

        $Lst.Get() |
        Select-Object -Property $Lst.GetPropertyOrder() |
        Export-Csv -Path $CsvPath -Encoding $Enc -NoTypeInformation
    }
}
